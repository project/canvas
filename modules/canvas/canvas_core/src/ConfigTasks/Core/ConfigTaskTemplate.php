<?php

/**
 * @file
 * Contains a ConfigTaskTemplate
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\canvas_core\ConfigTasks\Core;

use Drupal\canvas\ConfigTasks\ConfigTaskBase;
use Drupal\canvas\ConfigTasks\ConfigTaskInterface;

/**
 * Class ConfigTaskTemplate
 * @package Drupal\canvas_core\ConfigTasks\Core
 */
class ConfigTaskTemplate extends ConfigTaskBase implements ConfigTaskInterface {

  /**
   * Activate the configuration.
   */
  public function doConfig() {
    // TODO: Implement doConfig() method.
  }

  /**
   * Fetch the configuration parameters.
   *
   * @return mixed
   *   Parameters suitable for doConfig() to use.
   */
  public function getConfig() {
    // TODO: Implement getConfig() method.
  }

}
