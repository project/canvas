<?php

/**
 * @file
 * Contains a CanvasException
 *
 * @license GPL v2 http://www.fsf.org/licensing/licenses/gpl.html
 * @author Chris Skene chris at xtfer dot com
 * @copyright Copyright(c) 2015 Christopher Skene
 */

namespace Drupal\canvas\Exception;


/**
 * Class CanvasException
 * @package Drupal
 */
class CanvasException extends \Exception {

}
