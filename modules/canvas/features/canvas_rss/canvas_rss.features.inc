<?php
/**
 * @file
 * canvas_rss.features.inc
 */

/**
 * Implements hook_views_api().
 */
function canvas_rss_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
